/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.imgur;

import com.fernandobarillas.imgurparser.Constants;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by fb on 1/15/16.
 */
public class ImgurParserTest {
    private OkHttpClient mClient;
    private String[] imageUrls = {
//            "https://imgur.com/mwmiywD",  // 404, TODO: Handle 404 URLs
            "https://imgur.com/gallery/FY1AbSo", // GIF with no GIFV
            "https://imgur.com/gallery/IYJeDxq", // GIFV
            "https://imgur.com/gallery/UulWjHQ", // Single image with a gallery URL
    };
    private String[] albumUrls = {
            "https://imgur.com/gallery/fE92d", // Album with a gallery URL
            "https://imgur.com/a/cU6rs",       // Album with /a/ URL
            "https://imgur.com/a/tqIfU",       // Album with /a/ URL with image title/descriptions
    };

    public ImgurParserTest() {
        mClient = new OkHttpClient();
    }

    @Test
    public void getApiUrl() throws Exception {
        ImgurParser imgurParser = new ImgurParser(new URL("https://imgur.com/gallery/LOlginz"));
        assert imgurParser.getApiUrl().equals("https://api.imgur.com/2/image/LOlginz.json");

        imgurParser = new ImgurParser(new URL("https://imgur.com/gallery/wIege"));
        assert imgurParser.getApiUrl().equals("https://api.imgur.com/2/album/wIege.json");
    }

    @Test
    public void getImageUrl() throws Exception {
        for (String imageUrl : imageUrls) {
            if (Constants.DEBUG_ENABLED) {
                System.out.println(String.format("Image URL: %s", imageUrl));
            }

            ImgurParser imgurParser = new ImgurParser(new URL(imageUrl));
            assert !imgurParser.isAlbum();
            String apiUrl = imgurParser.getApiUrl();
            assert apiUrl != null;
            String jsonString = getJsonStringFromApi(apiUrl);
            assert jsonString != null;
            String resultUrl = imgurParser.getImageUrl(jsonString);
            assert resultUrl != null;
            assert !resultUrl.isEmpty();

            if (Constants.DEBUG_ENABLED) {
                System.out.println();
            }
        }
    }

    @Test
    public void getImageInformation() throws Exception {
        // TODO: Write me
    }

    @Test
    public void isAlbum() throws Exception {
        for (String imageUrl : imageUrls) {
            ImgurParser imgurParser = new ImgurParser(new URL(imageUrl));
            assert !imgurParser.isAlbum();
        }
        for (String albumUrl : albumUrls) {
            ImgurParser imgurParser = new ImgurParser(new URL(albumUrl));
            assert imgurParser.isAlbum();
        }
    }

    @Test
    public void getAlbum() throws Exception {
        // TODO: Write me
    }

    @Test
    public void getAlbumUrls() throws Exception {
        for (String albumUrl : albumUrls) {
            ImgurParser imgurParser = new ImgurParser(new URL(albumUrl));
            assert imgurParser.isAlbum();
            List<String> resultUrls = imgurParser.getAlbumUrls(getJsonStringFromApi(imgurParser.getApiUrl()));
            assert resultUrls != null;
            assert resultUrls.size() > 0;
        }
    }

    private String getJsonStringFromApi(String requestUrl) {
        if (Constants.DEBUG_ENABLED) {
            System.out.println("Request URL: " + requestUrl);
        }
        Request request = new Request.Builder()
                .url(requestUrl)
                .build();
        Response response = null;
        try {
            response = mClient.newCall(request).execute();
            String jsonString = response.body().string();

            if (Constants.DEBUG_ENABLED) {
                System.out.println(String.format("JSON Result: %s", jsonString));
            }
            return jsonString;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
