/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser;

//import com.fernandobarillas.imgurparser.imgur.ImgurParser;
//import com.fernandobarillas.imgurparser.reddit.RedditUrls;
//import com.squareup.okhttp.OkHttpClient;
//import com.squareup.okhttp.Request;
//import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Main {

    static String[] imageUrls = {
            "https://imgur.com/mwmiywD",  // 404
            "https://imgur.com/gallery/FY1AbSo", // GIF with no GIFV
            "https://imgur.com/gallery/IYJeDxq", // GIFV
            "https://imgur.com/gallery/UulWjHQ", // Single image with a gallery URL
            "https://imgur.com/gallery/fE92d",   // Album with a gallery URL
            "https://imgur.com/a/cU6rs",         // Album with /a/ URL
            "https://imgur.com/a/tqIfU",         // Album with /a/ URL with image title/descriptions
    };

    private static OkHttpClient sClient;

    public static void main(String[] args) {
        sClient = new OkHttpClient();

        testUrlArray();
//        testRedditImgurLinks();
    }

    private static String getJsonStringFromApi(String requestUrl) throws IOException {
        if (com.fernandobarillas.imgurparser.Constants.DEBUG_ENABLED) {
            System.out.println("Request URL: " + requestUrl);
        }
        Request request = new Request.Builder()
                .url(requestUrl)
                .build();
        Response response = sClient.newCall(request).execute();
        if (!response.isSuccessful()) {
            response.body().close();
//            throw new IOException();
            System.err.println("Unexpected code " + response);
            return null;
        }

        String jsonString = response.body().string();
        if (com.fernandobarillas.imgurparser.Constants.DEBUG_ENABLED) {
            System.out.println(String.format("JSON Result: %s", jsonString));
        }

        return jsonString;
    }

    private static void testUrlArray() {
        com.fernandobarillas.imgurparser.imgur.ImgurParser imgurParser = null;
        for (String imgurUrl : imageUrls) {
            try {
                URL url = new URL(imgurUrl);
                imgurParser = new com.fernandobarillas.imgurparser.imgur.ImgurParser(url);
                String resultUrl = imgurParser.getImageUrl(getJsonStringFromApi(imgurParser.getApiUrl()));
                if (com.fernandobarillas.imgurparser.Constants.DEBUG_ENABLED) {
                    if (resultUrl != null) {
                        System.out.println("Final URL: " + resultUrl);
                    } else {
                        System.err.println("Final URL was null");
                    }
                    System.out.println();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

/*
    private static void testRedditImgurLinks() {
        int validUrls = 0;
        com.fernandobarillas.imgurparser.reddit.RedditUrls redditUrls = new com.fernandobarillas.imgurparser.reddit.RedditUrls();
        List<String> parsedUrls;
        try {
            parsedUrls = redditUrls.getImgurUrls();
            for (String urlString : parsedUrls) {
                try {
                    URL url = new URL(urlString);
                    com.fernandobarillas.imgurparser.imgur.ImgurParser imgurParser = new com.fernandobarillas.imgurparser.imgur.ImgurParser(url);
                    String resultUrl = imgurParser.getImageUrl(getJsonStringFromApi(imgurParser.getApiUrl()));

                    if (com.fernandobarillas.imgurparser.Constants.DEBUG_ENABLED) {
                        if (resultUrl != null) {
                            System.out.println("Final URL: " + resultUrl);
                        } else {
                            System.out.println("Final URL was null");
                        }

                        System.out.println();
                    }

                    validUrls++;
                } catch (Exception e) {
                    System.err.println(String.format("Attempted URL: %s", urlString));
                    e.printStackTrace();
                }
            }

            System.out.println(String.format("Total Imgur URLS parsed: %d of %d total", validUrls, parsedUrls.size()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
*/
}
