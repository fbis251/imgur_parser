/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.reddit;

/**
 * Created by fb on 1/15/16.
 */
public class RedditUrls {
    /*
    private static final String SUBREDDIT = "pics";
    private static final String API_URL = "https://www.reddit.com/r/" + SUBREDDIT + "/.json?limit=100";

    private OkHttpClient mClient;

    public RedditUrls() {
        mClient = new OkHttpClient();
    }

    public List<String> getImgurUrls() throws IOException {
        Request request = new Request.Builder()
                .url(API_URL)
                .build();
        Response response = null;
        response = mClient.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String jsonString = response.body().string();
        if (Constants.DEBUG_ENABLED) {
            System.out.println(String.format("JSON Result: %s", jsonString));
        }
        List<String> regexMatches = new ArrayList<String>();
        Matcher matcher = Pattern.compile(", \"url\": \"(.*?)\"").matcher(jsonString);
        while (matcher.find()) {
            String urlString = matcher.group(1);
            URL url = new URL(urlString);
            if (url.getHost().equals("imgur.com") || url.getHost().endsWith(".imgur.com")) {
                regexMatches.add(urlString);
                if (Constants.DEBUG_ENABLED) {
                    System.out.println(url);
                }
            }
        }

        System.out.println(String.format("Got %d matches from reddit API", regexMatches.size()));
        return regexMatches;
    }
    */
}
