/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.imgur;

import com.fernandobarillas.imgurparser.Constants;
import com.fernandobarillas.imgurparser.exceptions.ImgurHashNullException;
import com.fernandobarillas.imgurparser.exceptions.InvalidImgurUrlException;
import com.fernandobarillas.imgurparser.exceptions.InvalidImgurlHashException;
import com.fernandobarillas.imgurparser.imgur.models.Album;
import com.fernandobarillas.imgurparser.imgur.models.AlbumResponse;
import com.fernandobarillas.imgurparser.imgur.models.ImageContainer;
import com.fernandobarillas.imgurparser.imgur.models.ImageResponse;
import com.google.gson.Gson;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by fb on 4/21/15.
 */
public class ImgurParser {

    private static final int MAX_IMGUR_HASH_LENGTH = 8;

    private static final int ALBUM_HASH_LENGTH = 5;

    private static final String IMGUR_API_BASE_URL = "https://api.imgur.com/2/";

    private static final String IMAGE_API_URL = IMGUR_API_BASE_URL + "image/";

    private static final String ALBUM_API_URL = IMGUR_API_BASE_URL + "album/";

    private static final String JSON_EXTENSION = ".json";

    private Gson mGson;

    private URL mImgurUrl;

    private boolean mIsAlbum;

    /**
     * Initializes the class based on the passed-in URL. Performs validation of the URL to make sure
     * that it's a link to an Imgur image or album
     *
     * @param url A URL to the Image/Album whose details should be gotten from the Imgur API
     * @throws ImgurHashNullException
     * @throws InvalidImgurUrlException
     * @throws InvalidImgurlHashException
     */
    public ImgurParser(URL url) throws ImgurHashNullException, InvalidImgurUrlException, InvalidImgurlHashException {
        mImgurUrl = url;
        validateImgurUrl(mImgurUrl);

        // Now that the imgur link has been validated, we're ready to parse the JSON String
        mGson = new Gson();

        // TODO: Add API error handling
    }

    /**
     * Gets the Imgur API URL based on the image/album hash of the URL passed in to the constructor
     *
     * @return The URL to the imgur API that can be used to get the image/album details as a JSON
     * response
     */
    public String getApiUrl() {
        String imgurId = getImgurHash();
        if (imgurId == null) {
            return null;
        }

        String apiUrl = mIsAlbum ? ALBUM_API_URL : IMAGE_API_URL;
        return apiUrl + imgurId + JSON_EXTENSION;
    }

    public String getImageUrl(String jsonString) {
        if (Constants.DEBUG_ENABLED) {
            System.out.println("getImageUrl( " + mImgurUrl.toString() + " )");
        }
        String resultUrl = null;
        ImageContainer imageResult = getImageInformation(jsonString);

        if (imageResult != null) {
            if (Constants.DEBUG_ENABLED) {
                System.out.println(imageResult);
            }

            resultUrl = imageResult.getLinks().getOriginal();
            // TODO: Convert GIF extension to GIFV
//            if (imageResult.getImage().isAnimated()) {
//                resultUrl = resultUrl.replace("gif", "gifv");
//            }

            if (Constants.DEBUG_ENABLED) {
                System.out.println(imageResult);
            }
        }

        return resultUrl;
    }

    public ImageContainer getImageInformation(String jsonString) {
        if (mIsAlbum) {
            // FIXME: Throw Exception
        }

        ImageResponse imageResponse = mGson.fromJson(jsonString, ImageResponse.class);
        if (imageResponse == null) {
            return null;
        }

        return imageResponse.getImageContainer();
    }

    public boolean isAlbum() {
        return mIsAlbum;
    }

    public Album getAlbum(String jsonString) {
        AlbumResponse albumResponse = mGson.fromJson(jsonString, AlbumResponse.class);
        return albumResponse.getAlbum();
    }

    public List<String> getAlbumUrls(String jsonString) {
        if (!mIsAlbum) {
            // FIXME: Throw exception here
        }
        Album album = getAlbum(jsonString);
        List<ImageContainer> albumImages = album.getImages();
        List<String> albumUrls = new ArrayList<>();
        if (!albumImages.isEmpty()) {
            if (Constants.DEBUG_ENABLED) {
                System.out.println(album.toString());
            }
            for (ImageContainer albumImage : albumImages) {
                albumUrls.add(albumImage.getLinks().getOriginal());
            }
        }

        return albumUrls;
    }

    public String getImgurHash() {
        String path = mImgurUrl.getPath();
        Pattern pattern = Pattern.compile("(?:(?:\\/gallery\\/)|(?:\\/a\\/))?(\\w{5,})");
        Matcher matcher = pattern.matcher(path);

        if (matcher.find()) return matcher.group(1);

        return null;
    }

    private void validateImgurUrl(URL url) throws InvalidImgurUrlException, ImgurHashNullException, InvalidImgurlHashException {
        String host = url.getHost();
        String path = url.getPath();

        if (!(host.equals("imgur.com") || host.endsWith(".imgur.com"))) {
            throw new InvalidImgurUrlException(url.toString());
        }

        if (getImgurHash() == null) {
            throw new ImgurHashNullException();
        }

        if (getImgurHash().length() > MAX_IMGUR_HASH_LENGTH) {
            throw new InvalidImgurlHashException(url.toString());
        }

        if (path.startsWith("/a/") || getImgurHash().length() == ALBUM_HASH_LENGTH) {
            mIsAlbum = true;
        }

        if (Constants.DEBUG_ENABLED) {
            System.out.println(String.format("host: %s", host));
            System.out.println(String.format("path: %s", path));
            System.out.println(String.format("is album: %b", mIsAlbum));
        }
    }
}
