package com.fernandobarillas.imgurparser.imgur.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fb on 2/12/16.
 */
public class Error {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("request")
    @Expose
    private String request;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("parameters")
    @Expose
    private String parameters;

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The request
     */
    public String getRequest() {
        return request;
    }

    /**
     * @param request The request
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /**
     * @return The method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method The method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @return The format
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format The format
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return The parameters
     */
    public String getParameters() {
        return parameters;
    }

    /**
     * @param parameters The parameters
     */
    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

}
