/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.imgur.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ImageContainer {

    @SerializedName("image")
    @Expose
    public Image image;
    @SerializedName("links")
    @Expose
    public Links links;

    @Override
    public String toString() {
        String result = "";

        if (image.getTitle() != null && !image.getTitle().isEmpty()) {
            result += String.format("Title: %s\n", image.getTitle());
        }
        if (image.getCaption() != null && !image.getCaption().isEmpty()) {
            result += String.format("Caption: %s\n", image.getCaption());
        }
        if (image.getDatetime() != null && !image.getDatetime().isEmpty()) {
            result += String.format("Date: %s\n", image.getDatetime());
        }
        if (image.getType() != null && !image.getType().isEmpty()) {
            result += String.format("Type: %s\n", image.getType());
        }
        if (image.getWidth() != null) {
            result += String.format("Width: %s\n", image.getWidth());
        }
        if (image.getHeight() != null) {
            result += String.format("Height: %s\n", image.getHeight());
        }
        if (image.getSize() != null) {
            result += String.format("Size: %s\n", image.getSize());
        }
        if (image.getViews() != null) {
            result += String.format("Views: %s\n", image.getViews());
        }
        if (image.getBandwidth() != null) {
            result += String.format("Bandwidth: %s\n", image.getBandwidth());
        }
        result += String.format("Animated: %b\n", image.isAnimated());
        result += String.format("URL: %s", links.getOriginal());
        return result;
    }

    public Image getImage() {
        return image;
    }

    public Links getLinks() {
        return links;
    }
}
