/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.imgur.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class Album {

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("cover")
    @Expose
    public String cover;
    @SerializedName("layout")
    @Expose
    public String layout;
    @SerializedName("images")
    @Expose

    private List<ImageContainer> images;

    @Override
    public String toString() {
        String result = "";

        if (title != null && !title.isEmpty()) {
            result += String.format("Title: %s\n", title);
        }
        if (description != null && !description.isEmpty()) {
            result += String.format("Description: %s\n", description);
        }
        if (cover != null && !cover.isEmpty()) {
            result += String.format("Cover: %s\n", cover);
        }
        if (layout != null && !layout.isEmpty()) {
            result += String.format("Layout: %s\n", layout);
        }

        result += String.format("Image Count: %d", images.size());
        return result;
    }

    public String getCover() {
        return this.cover;
    }

    public String getDescription() {
        return this.description;
    }

    public List<ImageContainer> getImages() {
        return this.images;
    }

    public String getLayout() {
        return this.layout;
    }

    public String getTitle() {
        return this.title;
    }
}
