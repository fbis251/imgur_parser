/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.imgur.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links {

    @SerializedName("original")
    @Expose
    private String original;
    @SerializedName("imgur_page")
    @Expose
    private String imgurPage;
    @SerializedName("small_square")
    @Expose
    private String smallSquare;
    @SerializedName("big_square")
    @Expose
    private String bigSquare;
    @SerializedName("small_thumbnail")
    @Expose
    private String smallThumbnail;
    @SerializedName("medium_thumbnail")
    @Expose
    private String mediumThumbnail;
    @SerializedName("large_thumbnail")
    @Expose
    private String largeThumbnail;
    @SerializedName("huge_thumbnail")
    @Expose
    private String hugeThumbnail;

    /**
     * @return The original
     */
    public String getOriginal() {
        return original;
    }

    /**
     * @param original The original
     */
    public void setOriginal(String original) {
        this.original = original;
    }

    /**
     * @return The imgurPage
     */
    public String getImgurPage() {
        return imgurPage;
    }

    /**
     * @param imgurPage The imgur_page
     */
    public void setImgurPage(String imgurPage) {
        this.imgurPage = imgurPage;
    }

    /**
     * @return The smallSquare
     */
    public String getSmallSquare() {
        return smallSquare;
    }

    /**
     * @param smallSquare The small_square
     */
    public void setSmallSquare(String smallSquare) {
        this.smallSquare = smallSquare;
    }

    /**
     * @return The bigSquare
     */
    public String getBigSquare() {
        return bigSquare;
    }

    /**
     * @param bigSquare The big_square
     */
    public void setBigSquare(String bigSquare) {
        this.bigSquare = bigSquare;
    }

    /**
     * @return The smallThumbnail
     */
    public String getSmallThumbnail() {
        return smallThumbnail;
    }

    /**
     * @param smallThumbnail The small_thumbnail
     */
    public void setSmallThumbnail(String smallThumbnail) {
        this.smallThumbnail = smallThumbnail;
    }

    /**
     * @return The mediumThumbnail
     */
    public String getMediumThumbnail() {
        return mediumThumbnail;
    }

    /**
     * @param mediumThumbnail The medium_thumbnail
     */
    public void setMediumThumbnail(String mediumThumbnail) {
        this.mediumThumbnail = mediumThumbnail;
    }

    /**
     * @return The largeThumbnail
     */
    public String getLargeThumbnail() {
        return largeThumbnail;
    }

    /**
     * @param largeThumbnail The large_thumbnail
     */
    public void setLargeThumbnail(String largeThumbnail) {
        this.largeThumbnail = largeThumbnail;
    }

    /**
     * @return The hugeThumbnail
     */
    public String getHugeThumbnail() {
        return hugeThumbnail;
    }

    /**
     * @param hugeThumbnail The huge_thumbnail
     */
    public void setHugeThumbnail(String hugeThumbnail) {
        this.hugeThumbnail = hugeThumbnail;
    }
}
