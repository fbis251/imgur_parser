package com.fernandobarillas.imgurparser.imgur.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fb on 2/12/16.
 */
public class ErrorResponse {

    @SerializedName("error")
    @Expose
    private Error error;

    /**
     * @return The error
     */
    public Error getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(Error error) {
        this.error = error;
    }

}
