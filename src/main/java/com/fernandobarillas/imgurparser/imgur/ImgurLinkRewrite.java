/*
 * Copyright (c) 2016 Fernando Barillas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.fernandobarillas.imgurparser.imgur;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by fb on 4/11/15.
 */
public class ImgurLinkRewrite {

    private static final String SMALL_SQUARE = "s";

    private static final String BIG_SQUARE = "b";

    private static final String SMALL_THUMBNAIL = "t";

    private static final String MEDIUM_THUMBNAIL = "m";

    private static final String LARGE_THUMBNAIL = "l";

    private static final String HUGE_THUMBNAIL = "h";

    private static final String sWhiteListDomains[] = {"i.imgur.com", "imgur.com"};

    public static String getSmallSquare(String url) {
        return rewriteLink(url, SMALL_SQUARE);
    }

    public static String getBigSquare(String url) {
        return rewriteLink(url, BIG_SQUARE);
    }

    public static String getSmallThumbnail(String url) {
        return rewriteLink(url, SMALL_THUMBNAIL);
    }

    public static String getMediumThumbnail(String url) {
        return rewriteLink(url, MEDIUM_THUMBNAIL);
    }

    public static String getLargeThumbnail(String url) {
        return rewriteLink(url, LARGE_THUMBNAIL);
    }

    public static String getHugeThumbnail(String url) {
        return rewriteLink(url, HUGE_THUMBNAIL);
    }

    public static String getHttpsUrl(String url) {
        if (getDomainFromUrl(url).toLowerCase().endsWith("imgur.com")) {
            return url.replace("http://", "https://");
        }

        return url;
    }

    private static String getDomainFromUrl(String url) {
        String result = "";

        URI uri;
        try {
            uri = new URI(url);
            result = uri.getHost();
        } catch (URISyntaxException e) {
        }

        return result;
    }

    private static String rewriteLink(String url, String desiredQuality) {
        String rewrittenUrl;
        String linkDomain = getDomainFromUrl(url);

        // Don't rewrite GIF links
        if (url.endsWith(".gif")) {
            return url;
        }

        // Make sure the image domain is in the whitelist
        boolean doReturn = true;
        for (String domain : sWhiteListDomains) {
            if (linkDomain.equals(domain)) {
                doReturn = false;
                break;
            }
        }

        if (doReturn) {
            return url;
        }

        // TODO: Perform rewrite of links that already have a set quality
        // TODO: Do a link rewrite only if the last character before the extension isn't one of the known size characters (b, t, m, etc). It seems that imgur ID's range from 5-8 characters now
        // We are going to replace any links to imgur to their "huge" quality equivalents
        String pattern = "(/[\\w]{7})(\\.[\\w]{3,4})";
        rewrittenUrl = url.replaceAll(pattern, "$1" + desiredQuality + "$2");

        return rewrittenUrl;
    }
}
