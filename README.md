Imgur Link Parser
=================

A Java project that allows you to parse links to imgur galleries and images
and retrieve a JSON object from the Imgur API.
